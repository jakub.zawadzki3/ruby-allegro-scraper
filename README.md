# Allegro archiwum scraper

Prosty scraper dla Archiwum Allegro z wykorzystaniem biblioteki Nokogiri.
Archiwum nie blokuje zapytań od botów, dla głównego serwisu dodanie user-agenta nie jest wystarczające.

## Wymagania

```bash
gem instal nokogiri
```

## Uruchomienie

```bash
ruby scraper.rb <kategoria-allegro> <nazwa_parametru_wyszukiwania>=<wartość_parametru_wyszukiwania>...
```
Przykładowo
```bash
ruby scraper.rb rtv-i-agd-10
```

Z parametrami:
```bash
ruby scraper.rb rtv-i-agd-10 price_from=107 price_to=140 marka=Braun stan=nowe
```

Po wykonaniu programu na konsoli zostaną wypisane produkty z ich dodatkowymi parametrami z podstrony oraz stworzony plik csv o nazwie links_list.csv z listą znalezionych produktów
