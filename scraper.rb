require 'nokogiri'
require 'open-uri'

NAME_ACCESOR = 'div > div > h2'
PRICE_ACCESOR = 'div > div:nth-child(3) > div > *' 
ROOT_URI = 'https://archiwum.allegro.pl/'


def product_basic_info (product)
  product_info = {}
  product_name = product.at_css(NAME_ACCESOR)
  product_price = product.at_css(PRICE_ACCESOR)
  product_info['name'] = product_name.content
  product_info['url'] = product_name.children[0].attributes['href'].content
  product_info['price'] = product_price.content
  return product_info
end

def product_details (product_path)
  product_details = {}
  product_doc = Nokogiri::HTML(URI.open(ROOT_URI + product_path))
  parameters_list = product_doc.at('h2:contains("Parametry")').parent
  param_names = parameters_list.css('dt')
  param_values = parameters_list.css('dd')
  param_names.zip(param_values).each do |param|
    product_details[param[0].content] = param[1].content
  end
  return product_details
end

category_uri = ROOT_URI + 'kategoria/' + ARGV[0] + '?'

ARGV[1..-1].each do|arg|
  category_uri << arg << '&'
end

puts 'Starting scraping for ' + category_uri
doc = Nokogiri::HTML(URI.open(category_uri))

list = doc.at_css('div.opbox-listing--base')
products = list.css('article > div')
File.open('./links_list.csv', 'w') { |output_file|
  products.each do |product|
    info = product_basic_info(product)
    details = product_details(info['url'])
    info['details'] = details
    puts info
    output_file.puts(info['name'] + ',' + ROOT_URI + info['url'])
  end
}

